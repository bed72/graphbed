import strawberry

from .types import User, Tweet
from ..database import get_users, get_tweets

@strawberry.type
class Query:
    all_users: list[User] = strawberry.field(
        resolver=get_users,
        name="get_users",
        description="Get all Users",
    )
    all_tweets: list[Tweet] = strawberry.field(
        resolver=get_tweets,
        name="get_tweets",
        description="Get all Tweets",
    )