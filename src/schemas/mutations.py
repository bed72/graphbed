import strawberry

from .types import User, Tweet
from ..database import create_user, create_tweet

@strawberry.type
class Mutation:
    create_user: User = strawberry.field(
        resolver=create_user,
        name="create_user",
        description="Create a new User",
    )
    create_tweet: Tweet = strawberry.field(
        resolver=create_tweet,
        name="create_tweet",
        description="Create a new Tweet",
    )