from uuid import UUID 

import strawberry

@strawberry.type
class User:
    id: UUID
    image: str
    username: str
    tweets: list["Tweet"]

@strawberry.type
class Tweet:
    id: UUID
    image: str
    message: str