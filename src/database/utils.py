import shutil

from strawberry.file_uploads import Upload

def handler_image(file: Upload) -> str:
    file_location: str = f"files/{file.filename}"

    with open(file_location, "wb+") as file_object:
        shutil.copyfileobj(file.file, file_object)  

    return file_location
