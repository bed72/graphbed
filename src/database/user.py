from strawberry.file_uploads import Upload

from ..models import User
from .utils import handler_image

async def get_users() -> list[User]:
    return await User.all()

async def create_user(image: Upload, username: str) -> User:
    return await User.create(image=handler_image(image), username=username)
