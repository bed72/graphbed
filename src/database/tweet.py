from uuid import UUID
from strawberry.file_uploads import Upload

from ..models import Tweet
from .utils import handler_image

async def get_tweets() -> list[Tweet]:
    return await Tweet.all()

async def create_tweet(user_id: UUID, image: Upload, message: str) -> Tweet:
    return await Tweet.create(image=handler_image(image), message=message, user=user_id)
