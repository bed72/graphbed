from tortoise import fields
from tortoise.models import Model

from .user import User

class Tweet(Model):
    id = fields.UUIDField(pk=True)
    image = fields.TextField()
    message = fields.CharField(max_length=255)
    is_published = fields.BooleanField(default=True)
    user = fields.ForeignKeyField("models.User", related_name="tweets")
    created_date = fields.DatetimeField(auto_now_add=True)

    def __str__(self):
        return self.message

    class Meta:
        table = "tweets"