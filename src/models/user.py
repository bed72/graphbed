from tortoise import fields
from tortoise.models import Model

class User(Model):
    id = fields.UUIDField(pk=True)
    image = fields.TextField()
    name = fields.CharField(max_length=32, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        table = "users"