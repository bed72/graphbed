from fastapi import FastAPI

from tortoise.contrib.fastapi import register_tortoise

def handler_database(app: FastAPI):
    register_tortoise(
        app,
        generate_schemas=True,
        db_url="sqlite://db.sqlite3",
        modules={"models": ["src.models"]},
    )