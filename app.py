from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from src.schemas import graphql_router
from src.models import handler_database

app = FastAPI(title="Bed", version="0.1.0", description="Bed App")
app.mount("/files", StaticFiles(directory="files"), name="files")
app.include_router(graphql_router, prefix="/graphql", tags=["graphql"])

handler_database(app)

@app.get("/")
def read_root():
    return {"Hello": "World"}